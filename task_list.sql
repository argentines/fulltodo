-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: tasklist
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `completed_count` bigint DEFAULT '0',
  `uncompleted_count` bigint DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_title` (`title`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (167,'Family',0,4),(168,'Wark',0,2),(169,'Buy',1,3),(170,'Relaxation',0,2),(171,'Travels',0,1),(173,'Training',0,3),(179,'Sport',0,2),(180,'Health',0,3),(182,'Pay',0,1),(188,'Sleep',0,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `priority`
--

DROP TABLE IF EXISTS `priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `priority` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priority`
--

LOCK TABLES `priority` WRITE;
/*!40000 ALTER TABLE `priority` DISABLE KEYS */;
INSERT INTO `priority` VALUES (56,'Low','#088836'),(58,'Hight','#e41f1f'),(73,'Middle','#e66b66');
/*!40000 ALTER TABLE `priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stat`
--

DROP TABLE IF EXISTS `stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stat` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `completed_total` bigint DEFAULT '0',
  `uncompleted_total` bigint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stat`
--

LOCK TABLES `stat` WRITE;
/*!40000 ALTER TABLE `stat` DISABLE KEYS */;
INSERT INTO `stat` VALUES (1,1,22);
/*!40000 ALTER TABLE `stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `completed` int DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `priority_id` bigint DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_idx` (`category_id`),
  KEY `fk_priority_idx` (`priority_id`),
  KEY `index_title` (`title`),
  KEY `index_completed` (`completed`),
  KEY `index_date` (`date`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `fk_priority` FOREIGN KEY (`priority_id`) REFERENCES `priority` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (328,'Call a parents',0,'2020-09-01 16:23:46',58,167),(331,'View a cartoons',0,'2020-04-27 15:27:29',NULL,167),(333,'Take Java courses',0,'2020-04-30 09:38:39',56,173),(338,'Make a green cocktail',0,'2020-04-27 15:27:34',56,180),(339,'Buy a loaf of bread',1,'2020-04-28 07:03:03',73,169),(341,'Call the boss',0,'2020-05-06 09:38:23',NULL,168),(342,'Measure pressure',0,'2020-05-01 09:38:46',NULL,180),(343,'Start running in the morning',0,NULL,56,179),(344,'Push out 100 times',0,NULL,58,179),(349,'Find educational games for kids',0,'2020-04-29 09:38:51',NULL,167),(350,'Buy medicine',0,'2020-04-30 09:38:43',56,180),(351,'Turn of Kotlin',0,'2020-05-06 09:38:37',73,173),(352,'View videos, how to build a house',0,NULL,NULL,173),(353,'Watch the series',0,'2020-04-29 09:38:29',NULL,170),(354,'Go to nature',0,'2020-04-15 18:00:00',NULL,170),(355,'Create a list of countries for travel',0,'2020-04-29 09:38:26',NULL,171),(356,'Finish reports',0,'2020-04-30 09:38:20',NULL,168),(367,'Renovar la tarjeta roja',0,'2018-05-05 00:00:00',56,167),(368,'Buy a loaf of milk',0,'2020-09-03 13:57:51',56,169),(369,'Go to bads',0,'2020-09-02 17:41:01',56,188),(371,'Factura de Gaz',0,'2020-09-14 22:00:00',58,182),(372,'Buy vitaminas',0,'2020-09-05 08:50:40',56,169),(373,'Buy Vitamina B',0,'2020-09-23 22:00:00',58,169);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `task_AFTER_INSERT` AFTER INSERT ON `task` FOR EACH ROW BEGIN
	
    if(ifnull(NEW.category_id, 0) > 0 && ifnull(NEW.completed, 0)=1) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
	end if;
    
    if(ifnull(NEW.category_id, 0) > 0 && ifnull(NEW.completed, 0)=0)  then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
	end if;
    
    
    if(ifnull(NEW.completed, 0)=1) then
		update tasklist.stat set completed_total = (ifnull(completed_total, 0)+1) where id = 1;
	else
		update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)+1) where id = 1;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `task_AFTER_UPDATE` AFTER UPDATE ON `task` FOR EACH ROW BEGIN
    
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1), completed_count = (ifnull(completed_count, 0)+1) where id = old.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
    end if;

    
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1), completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)+1), completed_total = (ifnull(completed_total, 0)-1) where id = 1;
    end if;

    
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

        update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
    end if;


    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;

        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
    end if;

    
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(completed_count, 0) -1) where id = old.category_id;

        update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `task_AFTER_DELETE` AFTER DELETE ON `task` FOR EACH ROW BEGIN
	
    if(ifnull(old.category_id, 0) > 0 && ifnull(old.completed, 0)=1) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;
	end if;
    
    if(ifnull(old.category_id, 0) > 0 && ifnull(old.completed, 0)=0) then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;
	end if;
    
    
    if(ifnull(old.completed, 0)=1) then
		update tasklist.stat set completed_total = (ifnull(completed_total, 0)-1) where id = 1;
	else
		update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1) where id = 1;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'tasklist'
--

--
-- Dumping routines for database 'tasklist'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-04 21:50:56
