//Trigers for tasklist
//Table task
AFTER INSERT
	task_AFTER_INSERT
	
	CREATE DEFINER=`root`@`localhost` TRIGGER `task_AFTER_INSERT` AFTER INSERT ON `task` FOR EACH ROW BEGIN
	
    if(ifnull(NEW.category_id, 0) > 0 && ifnull(NEW.completed, 0)=1) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
	end if;
    
    if(ifnull(NEW.category_id, 0) > 0 && ifnull(NEW.completed, 0)=0) then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
	end if;
    
    
    if(ifnull(NEW.completed, 0)=1) then
		update tasklist.stat set completed_total = (ifnull(completed_total, 0)+1) where id = 1;
	else
		update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)+1) where id = 1;
    end if;
END

//Table task
// AFTER UPDATE
	//task_AFTER_UPDATE
CREATE DEFINER=`root`@`localhost` TRIGGER `task_AFTER_UPDATE` AFTER UPDATE ON `task` FOR EACH ROW BEGIN
	
    /*1 changed complited on 1 but do not changed category*/
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1), completed_count = (ifnull(completed_count, 0)+1) where id = old.category_id;
        
        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
	end if;
    
    
    
    /*2 changed complited on 0 but do not changed category*/
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1), completed_count = (ifnull(completed_count, 0)+1) where id = old.category_id;
        
        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
	end if;
    


    
    /*3 changed category for complited=1*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

		update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
    end if;
    
    

    
     /*4 changed category for not changeed completed=0*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
		update tasklist.stat set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;

		update tasklist.stat set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
    end if;
    
    
    
    
    /*5 changed category, change completed from 1 on 0*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
        
        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)+1), completed_total = (ifnull(completed_total, 0)-1) where id = 1;
    end if;
    
   
    
     /*6 changed category, change completed from 0 on 1*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
		update tasklist.category set uncompleted_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

		update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
        
        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
    end if;
    
END


//Table task
//AFTER DELETE
  //task_AFTER_DELETE
  CREATE DEFINER=`root`@`localhost` TRIGGER `task_AFTER_DELETE` AFTER DELETE ON `task` FOR EACH ROW BEGIN
	
    if(ifnull(old.category_id, 0) > 0 && ifnull(old.completed, 0)=1) then
		update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;
	end if;
    
    if(ifnull(old.category_id, 0) > 0 && ifnull(old.completed, 0)=0) then
		update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;
	end if;
    
    
    if(ifnull(old.completed, 0)=1) then
		update tasklist.stat set completed_total = (ifnull(completed_total, 0)-1) where id = 1;
	else
		update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1) where id = 1;
    end if;
END
  
  
  
  
 //Last changed 
CREATE DEFINER=`root`@`localhost` TRIGGER `task_AFTER_UPDATE` AFTER UPDATE ON `task` FOR EACH ROW BEGIN
    /*1 changed complited on 1 but do not changed category*/
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1), completed_count = (ifnull(completed_count, 0)+1) where id = old.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
    end if;

    /*2 changed complited on 0 but do not changed category*/
    if(ifnull(old.completed, 0) <> ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) = ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1), completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)+1), completed_total = (ifnull(completed_total, 0)-1) where id = 1;
    end if;

    /*3 changed category for complited=1*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        update tasklist.category set completed_count = (ifnull(completed_count, 0)-1) where id = old.category_id;

        update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;
    end if;

    /*4 changed category for not changeed completed=0
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;

        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
    end if;*/

    /*5 changed category, change completed from 1 on 0*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=0 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        /*if (uncompleted_count <> 0) then*/
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)-1) where id = old.category_id;
        /*end if;*/
        update tasklist.category set uncompleted_count = (ifnull(uncompleted_count, 0)+1) where id = NEW.category_id;
    end if;

    /*6 changed category, change completed from 0 on 1*/
    if(ifnull(old.completed, 0) = ifnull(NEW.completed, 0) && NEW.completed=1 && ifnull(old.category_id, 0) <> ifnull(NEW.category_id, 0)) then
        update tasklist.category set uncompleted_count = (ifnull(completed_count, 0) -1) where id = old.category_id;

        update tasklist.category set completed_count = (ifnull(completed_count, 0)+1) where id = NEW.category_id;

        update tasklist.stat set uncompleted_total = (ifnull(uncompleted_total, 0)-1), completed_total = (ifnull(completed_total, 0)+1) where id = 1;
    end if;
END
