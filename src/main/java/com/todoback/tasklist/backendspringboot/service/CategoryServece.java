package com.todoback.tasklist.backendspringboot.service;


import com.todoback.tasklist.backendspringboot.entity.Category;
import com.todoback.tasklist.backendspringboot.repo.CategoryRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service

@Transactional
public class CategoryServece {

    private final CategoryRepository repository;

    public CategoryServece(CategoryRepository repository) {
        this.repository = repository;
    }

    public List<Category> findAll() {
        return repository.findAll();
    }

    public Category add(Category category) {
        return repository.save(category);
    }

    public Category update(Category category) {
        return repository.save(category);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public List<Category> findByTitle(String title) {
        return repository.findByTitle(title);
    }

    public Category findById(Long id) {
        return repository.findById(id).get();
    }

    public List<Category> findAllByOrderByTitleAsc() {
        return repository.findAllByOrderByTitleAsc();
    }
}
