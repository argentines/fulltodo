package com.todoback.tasklist.backendspringboot.service;

import com.todoback.tasklist.backendspringboot.entity.Task;
import com.todoback.tasklist.backendspringboot.repo.TaskRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service

@Transactional
public class TaskService {

    private final TaskRepository repository;

    public TaskService(TaskRepository taskRepository) {
        repository = taskRepository;
    }

    public List<Task> findAll() {
        return repository.findAllByOrderByTitleAsc();
    }

    public Task add(Task task) {
       return repository.save(task);
    }

    public Task update(Task task) {
       return repository.save(task);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Task findById(Long id) {
        return repository.findById(id).get();
    }

    public Page<Task> findByParams(String title, Integer completed, Long priorityId, Long categoryId, PageRequest pageRequest) {
        return repository.findByParams(title, completed, priorityId, categoryId, pageRequest);
    }
}
