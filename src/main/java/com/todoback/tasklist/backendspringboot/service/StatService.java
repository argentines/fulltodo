package com.todoback.tasklist.backendspringboot.service;

import com.todoback.tasklist.backendspringboot.entity.Stat;
import com.todoback.tasklist.backendspringboot.repo.StatRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service

@Transactional
public class StatService {

    private final StatRepository repository;

    public StatService(StatRepository statRepository) {
        this.repository = statRepository;
    }

    public Stat findById(Long id) {
        return repository.findById(id).get();
    }
}
