package com.todoback.tasklist.backendspringboot.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor

public class TaskSearchValue {

    private String title;
    private Integer completed;
    private Long priorityId;
    private Long categoryId;

    //pagination
    private Integer pageNumber;
    private Integer pageSize;

    //sort
    private String sortColumn;
    private String sortDirection;

    public String getTitle() {
        return this.title;
    }

    public Integer getCompleted() {
        return this.completed;
    }

    public Long getPriorityId() {
        return this.priorityId;
    }

    public Long getCategoryId() {
        return this.categoryId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

}
