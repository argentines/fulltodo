package com.todoback.tasklist.backendspringboot.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Basic;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class CategorySearchValue {

    private String title;

    public String getTitle() {
        return this.title;
    }
}
