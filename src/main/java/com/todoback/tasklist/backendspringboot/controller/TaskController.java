package com.todoback.tasklist.backendspringboot.controller;

import com.todoback.tasklist.backendspringboot.entity.Task;
import com.todoback.tasklist.backendspringboot.repo.TaskRepository;
import com.todoback.tasklist.backendspringboot.search.TaskSearchValue;
import com.todoback.tasklist.backendspringboot.service.TaskService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/task")
@CrossOrigin(origins = "http://localhost:4200")
public class TaskController {

    private final TaskService taskService;

    private TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/all")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @PostMapping("/add")
    public ResponseEntity<Task> add(@RequestBody Task task) {

        if(task.getId() != null && task.getId() != 0) {
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        if(task.getTitle() == null || task.getTitle().trim().length() == 0) {
            return new ResponseEntity("missed param title", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(taskService.add(task));
    }

    @PutMapping("/update")
    public ResponseEntity<Task> update(@RequestBody Task task) {

        if (task.getId() == null || task.getId() == 0) {
            return new ResponseEntity("The id hasn't be nullable", HttpStatus.NOT_ACCEPTABLE);
        }

        if(task.getTitle() == null || task.getTitle().trim().length() == 0) {
            return new ResponseEntity("The Title for update hasn't be nullable", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(taskService.update(task));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        try{
            taskService.deleteById(id);
        }catch(EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("The id "+id+" not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Task> findById(@PathVariable Long id) {

        Task task = null;
        try{
           task =  taskService.findById(id);
        }catch(NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity("The id "+id+" not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(task);
    }

    @PostMapping("/search")
    public ResponseEntity search(@RequestBody TaskSearchValue taskSearchValue) {

        //imitateLoading();

        //search
        String title = taskSearchValue.getTitle() != null ? taskSearchValue.getTitle() : null;
        Integer completed = taskSearchValue.getCompleted() != null ? taskSearchValue.getCompleted() : null;
        Long priorityId = taskSearchValue.getPriorityId() != null ? taskSearchValue.getPriorityId() : null;
        Long categoryId = taskSearchValue.getCategoryId() != null ? taskSearchValue.getCategoryId() : null;

        //sort
        String sortColumn = taskSearchValue.getSortColumn() != null ? taskSearchValue.getSortColumn() : null;
        String sortDirection = taskSearchValue.getSortDirection() != null ? taskSearchValue.getSortDirection() : null;

        //pagination
        Integer pageNumber = taskSearchValue.getPageNumber() != null ? taskSearchValue.getPageNumber() : null;
        Integer pageSize = taskSearchValue.getPageSize() != null ? taskSearchValue.getPageSize() : null;

        Sort.Direction direction = sortDirection == null || sortDirection.trim().length() == 0 || sortDirection.trim().equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

        //object sort
        Sort sort = Sort.by(direction, sortColumn);

        //object pagination
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, sort);
        Page result = taskService.findByParams(title, completed, priorityId, categoryId, pageRequest);

        return ResponseEntity.ok(result);
    }

    private void imitateLoading() {
        try {
            Thread.sleep(700);
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
