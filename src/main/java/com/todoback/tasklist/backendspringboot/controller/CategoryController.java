package com.todoback.tasklist.backendspringboot.controller;

import com.todoback.tasklist.backendspringboot.entity.Category;
import com.todoback.tasklist.backendspringboot.repo.CategoryRepository;
import com.todoback.tasklist.backendspringboot.search.CategorySearchValue;
import com.todoback.tasklist.backendspringboot.service.CategoryServece;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/category")
@CrossOrigin("http://localhost:4200")
public class CategoryController {

    private CategoryServece categoryServece;

    private CategoryController(CategoryServece categoryServece) {
        this.categoryServece = categoryServece;
    }

    @GetMapping("/all")
    public List<Category> findAll() {
       return categoryServece.findAllByOrderByTitleAsc();
    }

    @PostMapping("/add")
    public ResponseEntity<Category> add(@RequestBody Category category) {

        if(category.getId() != null && category.getId() != 0) {
            return new ResponseEntity("redundant param: id MUST be null", HttpStatus.NOT_ACCEPTABLE);
        }

        if(category.getTitle() == null || category.getTitle().trim().length() == 0) {
            return new ResponseEntity("missed param title", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(categoryServece.add(category));
    }

    @PutMapping("/update")
    public ResponseEntity<Category> update(@RequestBody Category category) {

        if (category.getId() == null || category.getId() == 0) {
            return new ResponseEntity("The id hasn't be nullable", HttpStatus.NOT_ACCEPTABLE);
        }

        if(category.getTitle() == null || category.getTitle().trim().length() == 0) {
            return new ResponseEntity("The Title for update hasn't be nullable", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(categoryServece.update(category));
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Category> findById(@PathVariable Long id) {
        Category category = null;

        try {
            category = categoryServece.findById(id);
        }catch (NoSuchElementException e) {
            e.printStackTrace();
            return new ResponseEntity("The id "+id+" not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return ResponseEntity.ok(category);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        try {
            categoryServece.deleteById(id);
        }catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity("The id "+id+" not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Category>> search(@RequestBody CategorySearchValue categorySearchValue){
        return ResponseEntity.ok(categoryServece.findByTitle(categorySearchValue.getTitle()));
    }
}
