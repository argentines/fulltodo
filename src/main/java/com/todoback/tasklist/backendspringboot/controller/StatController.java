package com.todoback.tasklist.backendspringboot.controller;

import com.todoback.tasklist.backendspringboot.entity.Stat;
import com.todoback.tasklist.backendspringboot.repo.StatRepository;
import com.todoback.tasklist.backendspringboot.service.StatService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("http://localhost:4200")
public class StatController {

    private final StatService statService;
    private final Long defaulId = 1l;

    private StatController(StatService statService) {

        this.statService = statService;
    }

    @GetMapping("/stat")
    public ResponseEntity<Stat> findById() {
        return ResponseEntity.ok(statService.findById(defaulId));
    }
}
